# Changelog

## [unreleased]

## [a17.6.0] - 2017-09-10
* It is possible to use both Normal and Rare CompTick.
* Compatible with CompRefuelable, CompPowerTrader and CompFlickable. They can
switch the building on/off
* Weather can influence production speed (namely sunshine, rain, snow and wind).
It does not cause ingredients to rot, just changes the speed.
* Add a button in dev mode for printing current weather speed factors and roofed
factor.
* Fix: Incorrect bar display when partially filled.

## [a17.5.0] - 2017-07-17
* All functionality in CompUniversalFermenter (no building class needed).
* Better inspection string (max. 5 lines).

## [a17.4.2] - 2017-07-16
* Fixed a derp with missing Static_Bar.cs file

## [a17.4.1] - 2017-07-16
* Added ingredients summary now trims vowels when max. line length is exceeded.
* Using static class instead of [StaticConstructorOnStartup].

## [a17.4.0] - 2017-07-09
* More readable XML format.
* Ingredients are set up by ThingFilter allowing multiple ingredients per
product.
* New <efficiency> property allowing different ingredient and product counts

## [a17.3.0] - 2017-06-12
* All vanilla fermenting barrel's parameters are now settable in XML (for each
product).

## [a17.2.0] - 2017-06-09
* Possible to set up more ingredient-product pairs.
* New ingame button for cycling through the products.

## [a17.1.0] - 2017-06-07
* First release: New building class and a component property based on a
fermenting barrel.
* Possible to set up a product to be made from one ingredient ThingDef.