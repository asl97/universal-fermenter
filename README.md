# Universal Fermenter #

<img src="https://gitlab.com/rwmods/universal-fermenter/raw/master/About/preview.png" width="800">

Universal Fermenter is a mod which allows you to create a custom "waiting"
production buildings in a style of the vanilla fermenting barrel. The mod adds
the possibility customize the building's parameters such as input ingredients,
output product, temperature ranges, fermentation speed and an influence of
weather on the speed. It is possible to assign multiple products to a single
building and cycle through them in game. Adding a new fermenter building is
simple and does not require any C# skills.

[Forum link] (https://ludeon.com/forums/index.php?topic=33398.0): I post progress updates there.

# How to Use #

By itself the mod doesn't add any new buildings to the game. It introduces a new
component property which you can add to some building class and customize it
from XML. For examples and detailed information of setable parameters see the
[mod Wiki](https://gitlab.com/rwmods/universal-fermenter/wikis/home).

# Download #
[Latest version (a17.6.0)](https://gitlab.com/rwmods/universal-fermenter/uploads/6e6cd4558f9a9ce97cf91a61fe811333/UniversalFermenter_a17.6.0.zip)

[All versions](https://gitlab.com/rwmods/universal-fermenter/tags)


# Co-authors #
cuproPanda: I used his
[Cupro's Drinks](https://ludeon.com/forums/index.php?topic=32190) which served
as a base for this mod.

# License #
This mod is licensed under the MIT license. You can use this mod for any purpose
and release it under any license, just give me a credit.